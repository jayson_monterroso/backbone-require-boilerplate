/**
 * @name templates
 * @description Use the Require.js text! plugin, to load raw text which will be used as the views primary templates.
 */
 define([
    'text!../templates/header.html',
    'text!../templates/footer.html',
    'text!../templates/video.html',
    'text!../templates/audio.html',
    'text!../templates/home.html'
    ],
    function (
        header,
        footer,
        video,
        audio,
        home
        ){
        var Templates = {
            header:header,
            footer:footer,
            video:video,
            audio:audio,
            home:home
        };
        return Templates;
    });