/**
 * @name app
 * @description Application main module. Load the router module and initialize it
 */
 define(['router'], function (AppRouter) {
 	return {
 		initialize: function () {
 			//global app declaration
 			app = new AppRouter();
 			Backbone.history.start();
 		}
 	};
 });