define(['utils', 'jquery','media'],
    function () {
        VideoView = Backbone.View.extend({
            tagName: 'div',
            className: 'video',
            initialize: function () {
                this.closed = false;
            },
            render: function () {

            },
            continueVideo: function () {
                Utils.continueVideo();
            },
            nextScreen: function () {
                this.insequence = true;
                app.view.video_sequense_continue();
            },
            finalScreen: function () {
                this.insequence = false;
                console.log('finalScreen');
                app.view.video_sequense_final();
            },
            play_video: function (id) {
                Utils.playVideo(id);
            },
            show_video: function () {
            //this.remove();
            //this.unbind();
            //$(this.el).removeClass('hidden');//.show();

        },
        close: function () {

        }
    });
});