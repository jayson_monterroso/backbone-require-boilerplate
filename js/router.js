/**
 * @name router
 * @description Define URL routes
 */
 define([
    'jquery',
    'underscore',
    'backbone'
    ],
    function($, _, Backbone){
        var AppRouter = Backbone.Router.extend({
            initialize: function() {
                //declare the constants in page templates.
                //ex. sidebar etc
                this.header = new HeaderView();
                this.footer = new FooterView();
            },
            routes: {
                ""                      :"show_page",
                "page/:id"              :"show_page",
                "*actions"              :"defaultRoute"
            },
            showStaticViews: function(){
                $('body').append(this.header.render());
                $('body').append('<div class="wrapper"></div>');
                $('body').append(this.footer.render());
            },
            show_page:function(id){
                var str = id+'View';
                console.log('str',str);
                if(window[str]){
                    var page = new window[str](id);
                    app.showView(page);
                }else{
                    console.log('Invalid view name or not defined');
                }
            },
            showView: function(view){
                if(app.view){
                    app.view.close();
                }
                if(!app.header.rendered || !app.footer.rendered){
                    this.showStaticViews();
                }
                app.view = view;
                app.view_url = location.hash;
                view.render();
            },
            defaultRoute:function(){
            }
        });
return AppRouter;
}
);
