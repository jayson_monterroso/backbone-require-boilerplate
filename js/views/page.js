define(['jquery', 'underscore', 'backbone'],
    function () {
        //parent page
        Page = Backbone.View.extend({
            className:'wrapper',
            config: {},
            close: function () {
                this.remove();
                this.unbind();
            },
            hide: function () {
                $(this.el).hide();
            },
            show: function () {
                //define how it going to be showed
                $(this.el).fadeIn(100);
            },
            back: function(e) {
                location.hash = app.view.config.back_url;
                return false;
            },
            next:function(e){
                if(app.view.continue_auto && app.view.config.sequence){
                    var list = app.view.config.sequence.split(','),
                    i_let = list.indexOf(app.view.subid);
                    if(list[i_let+1])
                        location.hash = '#/page/' + app.view.id + '' + list[i_let+1];
                    else
                        location.hash = app.view.config.next_url;
                }else{
                    location.hash = app.view.config.next_url;
                }
                return false;
            },
            disableNext:function(e){
                //disable the next button if is needed
            },
            enableNext:function(e){
                //enable next button
            },
            disableBack:function(e){
               //disableBack button
           },
           enableBack:function(e){
           },
           onRendered: function(){
            app.header.render();
            app.footer.render();
            this.rendered = true;
            var id_page = this.id;
            if(this.config.header.next_btn){
                $(app.header.el).find('.nav_arrow.next_btn').bind('click', this.next);
            }else{
                $(app.header.el).find('.nav_arrow.next_btn').off('click');
            }
            if(this.config.header.back_btn){
                $(app.header.el).find('.nav_arrow.back_btn').bind('click', this.back);
            }else{
                $(app.header.el).find('.nav_arrow.back_btn').off('click');
            }
        },
        page_set: function(args){
            this.config = Config.config_default();
            for(var key in args){
                var k = args[key];
                if(k){
                    if(typeof k == 'object'){
                        for(var keyH in k){
                            if(this.config[key][keyH] !== undefined){
                                this.config[key][keyH] = k[keyH];
                            }
                        }
                    }else{
                        this.config[key] = k;
                    }
                    if(key == 'bg_class'){
                        $('body').attr('class', '');
                        $('body').addClass(k);
                    }
                }
            }
            if(!this.config.next_url){
                this.config.next_url = '#/page/'+ (parseInt(this.id,10) + 1);
            }
            if(!this.config.back_url){
                this.config.back_url = '#/page/'+ (parseInt(this.id,10) - 1);
            }
        },
        page_wrap: function () {
            var w = $('body').outerHeight();
            if(!$('.wrapper').hasClass('noheader')){
                w -= $(app.header.el).outerHeight();
            }
            if(!$('.wrapper').hasClass('nofooter')){
                w -= $(app.footer.el).outerHeight();
            }
            $('.wrapper').css({
                height: w + 'px'
            });
        }
    });
});
