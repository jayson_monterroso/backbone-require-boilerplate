define(['jquery', 'underscore', 'backbone'],
    function () {
        FooterView = Backbone.View.extend({
            tagName: 'footer',
            rendered: false,
            config: function(){
                return Config.config_default().footer;
            },
            events: {
                "click .show-modal":"showModal"
            },
            initialize:function(){
            },
            render:function () {
                if(app.view){
                    this.config = app.view.config.footer;
                    this.config.progress = parseInt(app.view.id,10);
                    this.config.cant_pages = Config.cant_pages;
                }
                $(this.el).html(_.template(Templates.footer, {
                    config: this.config
                }));
                this.rendered = true;
                if(!this.config.display){
                    $(this.el).addClass('hidden');
                    $('.wrapper').addClass('nofooter');
                }else{
                    $(this.el).removeClass('hidden');
                    $('.wrapper').removeClass('nofooter');
                }
                return $(this.el);
            },
            showModal:function(e){
                e.preventDefault();
                try{
                    var target = $(e.currentTarget),
                    modal_id = target.data('modal-id'),
                    content = this.getModalContent(modal_id);
                    if(!content) {
                        console.log('no content');
                        return;
                    }
                    $.modal(content,{
                        onOpen: function (dialog) {
                            dialog.overlay.fadeIn('fast', function () {
                                dialog.container.fadeIn('fast');dialog.data.fadeIn('slow');
                            });
                        },
                        overlayClose:true
                    });
                    $.modal(content);
                }catch(error){
                    console.log(error);
                }
            },
            getModalContent:function(id){
                var content;
                switch(id){
                    case "privacy":
                    content =  Pages.global.privacy_content;
                    break;
                    case "terms":
                    content = Pages.global.terms_content;
                    break;
                }
                return content;
            }
        });
});