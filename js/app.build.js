({
    appDir: "../",
    baseUrl: "js",
    dir: "../optimized",
    //Optimize everything being in true
    // optimizeAllPluginResources: true,
    optimizeAllPluginResources: true,
    // optimize: "none",
    //optimize code
    optimize: "uglify",
    //optimize: "closure",
    //remove comments for plugins, like jquery
    // preserveLicenseComments: false,
    modules: [
    {
        name: "main"
    }
    ],
    paths:{
       jquery: 'libs/jquery-1.7.2.min',
       underscore: 'libs/underscore-min',
       backbone: 'libs/backbone-min',
       text: 'libs/text-2.0.1',
       json2:'libs/json2'

   }
})
