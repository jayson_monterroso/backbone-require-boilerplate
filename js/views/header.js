define(['jquery', 'underscore', 'backbone'],
    function () {
        HeaderView = Backbone.View.extend({
            tagName: 'header',
            rendered: false,
            config: function(){
                return Config.config_default().header;
            },
            events: {
            },
            initialize:function(){
            },
            render:function () {
                if(app.view)
                    this.config = app.view.config.header;
                $(this.el).html(_.template(Templates.header, {
                    config: this.config
                }));
                this.rendered = true;

                if(!this.config.display){
                    $(this.el).addClass('hidden');
                    $('.wrapper').addClass('noheader');
                }else{
                    $(this.el).removeClass('hidden');
                    $('.wrapper').removeClass('noheader');
                }

                return $(this.el);
            }
        });
    }
    );