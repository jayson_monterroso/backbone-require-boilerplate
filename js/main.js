// RequireJS Configuration. It defines the shortcut alias for the main libraries.
require.config({
    paths:{
        jquery: 'libs/jquery-1.7.2.min',
        underscore: 'libs/underscore-min',
        backbone: 'libs/backbone-min',
        text: 'libs/text-2.0.1',
        json2:'libs/json2'
    },
    shim: {
        'underscore': {
            deps: ['jquery']
        },
        'backbone': {
            deps: ['jquery', 'underscore']
        },
        'app':{
            deps: ['jquery', 'underscore', 'backbone']
        },
        'utils':{
            deps: ['jquery', 'underscore', 'backbone']
        },
        'router':{
            deps: ['jquery', 'underscore', 'backbone']
        }
    }
});
require([
    'app',
    'router',
    'globals',
    'views/video',
    'views/audio',
    'views/page',
    'views/header',
    'views/footer',
    'views/home'
    ],
    function (App,
        AppRouter,
        Utils,
        VideoView,
        AudioView,
        Page,
        Header,
        Footer,
        homeView) {
        App.initialize();
    });
