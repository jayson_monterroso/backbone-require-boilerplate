define(['views/page','templates'],
    function (pages,Templates) {
        console.log(Page);
        console.log(Templates);
        homeView = Page.extend({
            events:{
                "click .click_me":"alert"
            },

            initialize: function(id){
                console.log(id,'id');
                this.id = id;
                this.page_set({
                    header: {
                        display: true
                    },
                    footer: {
                        display: true
                    },
                    bg_class: 'home-screen'
                });
            },
            alert:function(e){
                e.preventDefault();
                alert('clicked');

            },
            render:function (args) {
               var config = {};
               config.id=  args;
               console.log(this.el,'this.el');
               var objeto = {
                title:'hola',
                body:'<a class="click_me">hola mundo</p>'
            };
            $(this.el).html(_.template(Templates[this.id], {texto:objeto}));
            $('.wrapper').append(this.el);
            this.onRendered();
        }
    });
    });