define(['utils', 'jquery','media'],
    function () {
        AudioView = Backbone.View.extend({
            tagName: 'div',
            className: 'audio',

            initialize: function(){
                this.closed = false;
            },
            render:function () {
                $(this.el).html(_.template(Templates.audio, {player: Utils.is_ipad}));
                $('body').append($(this.el));
                if (Utils.is_ipad) {
                 Media.initialize_player_object('audio');
                 Media.player_config();
             } else {
                Utils.displaySwf(Config.url_flash()+"audio_playlistplayer.swf?var="+new Date().getTime(), "altContent_audio", "286", "210" , this.flashvars, this.params, this.swfattributes);
            }
        }

    });
    });
