define(['jquery'], function ($) {
    Media = { //Jplayer Functions
        initialize_player_object: function () {
            playerFn = {
                volume:false,
                rwaction: function () {},
                ffaction: function () {},
                el: '#player',
                parent: this,
                play: $(this.el).jPlayer('play'),
                pause: $(this.el).jPlayer('pause'),
                rewinding: false,
                fastforward: false,
                isMuted: function () {
                    return $(this.el).jPlayer('option').muted;
                },
                getDuration: function () {
                    return $(this.el).data("jPlayer").status.duration;
                },
                getCurrentTime: function () {
                    return $(this.el).data("jPlayer").status.currentTime;
                },
                getCurrentVolume: function () {
                    return $(this.el).jPlayer('option').volume;
                },
                rewindTrack: function () {
                    //Get current progress and decrement
                    var currentProgress = this.getPlayerProgress(),
                        //Rewinds 10% of track length
                        futureProgress = currentProgress - 10;
                        console.log(currentProgress);
                    //If it goes past the starting point - stop rewinding and pause
                    if (futureProgress <= 0) {
                        this.rewinding = false;
                        window.clearInterval(this.rwaction);
                        $(this.el).jPlayer("pause", 0);
                    }
                    //Continue rewinding
                    else {
                        $(this.el).jPlayer("playHead", parseInt(futureProgress, 10));
                    }
                },
                forwardTrack: function () {
                    //Get current progress and increment
                    var currentProgress = this.getPlayerProgress(),
                    futureProgress = currentProgress + 5;
                    //If the percentage exceeds the max - stop fast forwarding at the end.
                    if (futureProgress >= 100) {
                        this.fastforward = false;
                        window.clearInterval(this.ffaction);
                        $(this.el).jPlayer("playHead", parseInt($('.jp-duration').text().replace(':', ''), 10));
                    } else {
                        $(this.el).jPlayer("playHead", parseInt(futureProgress, 10), 10);
                    }
                },
                getPlayerProgress: function () {
                    return (this.getCurrentTime() / this.getDuration() * 100);
                },
                setCurrentVolume: function (muted) {
                    var maxWidthBar = 61,
                    currentVolume = this.getCurrentVolume(),
                    position = (muted) ? 0 : maxWidthBar * currentVolume;
                    $('.indicator').css('left', position + 'px');
                },
                play_video: function (id, auto) {
                    var play_pause = (!auto) ? 'pause' : 'play',
                    video, video_file;
                    video_file = this.get_media_file(id);
                    $(this.el).jPlayer("setMedia", {
                        m4v: Config.url_video_ipad() + video_file.file
                    }).jPlayer(play_pause);
                    //Enable cuePoints function
                    this.cuePointsFn = false;
                },
                //play video fn
                play_audio: function (id, auto) {
                    var play_pause = (!auto) ? 'pause' : 'play',
                    video, media_file;
                    media_file = this.get_media_file(id, app.view.config.audio.display);
                    $(this.el).jPlayer("setMedia", {
                      mp3:Config.url_audio+media_file.file,
                      poster: Config.url_audio_thumbnail+media_file.video.thumbnail
                  }).jPlayer(play_pause);
                },
                get_media_file: function (id, audio) {
                    var load_media = (audio) ? Utils.audios_json.audios : Utils.videos_json.videos,
                    video_file, ret;
                    for (var i in load_media) {
                        media = load_media[i];
                        if (media.id === id) {
                            if(app.view.config.audio.display){
                                media_file = media.audio;
                            }else{
                                media_file = media.file.slice(0, -3) + 'm4v';
                            }
                            ret = {
                                video: media,
                                file: media_file
                            };
                            return ret;
                        }
                    }
                },
                showVolume: function () {
                    $('.volume').removeClass('hidden');
                },
                showProgressBar: function (e) {
                    var currentTime = e.jPlayer.status.currentTime,
                    duration = e.jPlayer.status.duration,
                    maxWidth = 180,
                    percentCover = currentTime / duration,
                    position = maxWidth * percentCover;
                    if (duration === currentTime) {
                        position = 0;
                    }
                    $('.play-indicator').css('left', position + 'px');
                },
                cuePoints: function (e) {
                    if (this.cuePointsFn) return;
                    var id =app.view.id,
                    videoInfo = this.get_media_file(id);
                    if (videoInfo.video.cuepoints) {
                        var cuePoints = videoInfo.video.cuepoints[0],
                        currentTime = Math.round(e.jPlayer.status.currentTime);
                        if (cuePoints.seconds == currentTime) {
                            console.log(cuePoints.function_name);
                            window[cuePoints.function_name]();
                            this.cuePointsFn = true;
                            return;
                        }
                    }
                }
            };
        },
        player_config: function () {
            this.setPlayerObject();
            this.setEvents();
            if(playerFn.volume){
                playerFn.setCurrentVolume();
            }
        },
        setPlayerObject:function(){
         var id = app.view.id,
         media_file = playerFn.get_media_file(id,app.view.config.audio.display),
         play_pause,file,size,
         is_audio = app.view.config.audio.display;
         if(is_audio){
             file = {
                mp3:Config.url_audio+media_file.file,
                poster: Config.url_audio_thumbnail+media_file.video.thumbnail
            };
            size =  {
                width: "auto",
                height: "auto",
                cssClass: "none"
            };
        }else{
            file = {
                m4v: Config.url_video_ipad() + media_file.file
            };
            size =  {
                width: "230px",
                height: "145px",
                cssClass: "none"
            };
        }
        console.log('media_file',media_file);
        console.log(file,'file')
        if (Utils.get_cookie('video_' + id) == "true"){play_pause = 'pause';}
        else{ play_pause = 'play';}
        $(playerFn.el).jPlayer({
            swfPath: "js",
            supplied: "m4v,mp3",
            size:size,
            ready: function () {
                $(this).jPlayer("setMedia",file ).jPlayer(play_pause);
                    //Auto Play for devices
                    var click = document.ontouchstart === undefined ? 'click' : 'touchstart',
                    kickoff = function () {
                        $(playerFn.el).jPlayer("play");
                        document.documentElement.removeEventListener(click, kickoff, true);
                    };
                    document.documentElement.addEventListener(click, kickoff, true);
                },
                ended: function (e) {
                    //end of reproduce video
                    if(!is_audio){
                        nextScreen_video();
                    }else{
                        nextScreen_audio();
                    }
                },
                volumechange: function (e) {
                    if (!event.srcElement.muted) {
                        playerFn.setCurrentVolume();
                    } else {
                        playerFn.setCurrentVolume('muted');
                    }
                },
                timeupdate: function (e) {
                    playerFn.showProgressBar(e);
                    if(!app.view.config.audio.display){
                        playerFn.cuePoints(e);
                    }
                }
            });
},
setEvents:function(){
    $('.play').click(function (e) {
        $(playerFn.el).jPlayer('play');
    });
    $('.pause').click(function (e) {
        $(playerFn.el).jPlayer('pause');
    });
    $('.rewind').mousedown(function (e) {
        if (!playerFn.rewinding) {
            playerFn.rewinding = true;
            $(playerFn.el).jPlayer("play");
            playerFn.rewindTrack();
            playerFn.rwaction = window.setInterval(
                function () {
                    playerFn.rewindTrack();
                }, 500);
        }
    }).mouseup(function (e) {
        playerFn.rewinding = false;
        window.clearInterval(playerFn.rwaction);
        $(playerFn.el).jPlayer("play");
    });
    $('.stop').click(function () {
        $(playerFn.el).jPlayer('stop');
    });
    $('.mute').click(function () {
        if (playerFn.isMuted()) {
            $(playerFn.el).jPlayer('unmute');
        } else {
            $(playerFn.el).jPlayer('mute');
            $(this).addClass('muted');
            $('.indicator').css('left', 0);
        }
    });
    $('.forward').mousedown(function (e) {
        if (!playerFn.fastforward) {
            playerFn.fastforward = true;
                    //Pause the player
                    $(playerFn.el).jPlayer("play");
                    playerFn.forwardTrack();
                    playerFn.ffaction = window.setInterval(function () {
                        playerFn.forwardTrack();
                    }, 500);
                }
            }).mouseup(function (e) {
                playerFn.fastforward = false;
                window.clearInterval(playerFn.ffaction);
                $(playerFn.el).jPlayer("play");
            });
            $('.volume-bar').click(function (e) {
                var target = $(e.currentTarget),
                maxWidth = $(this).css("width").slice(0, -2),
                clickPos = e.pageX - target.offset().left,
                percentage = clickPos / maxWidth * 100,
                volume = percentage / 100;
                $(playerFn.el).jPlayer("volume", volume);
            });
            $('.seek-bar').click(function (e) {
                e.preventDefault();
                var target = $(e.currentTarget),
                duration = playerFn.getDuration(),
                maxWidth = $(this).css("width").slice(0, -2),
                clickPos = e.pageX - target.offset().left,
                percentage = (clickPos / maxWidth * 100) / 100,
                time = percentage * duration;
                $(playerFn.el).jPlayer("play", time);
            });
        }
    };
});